package com.example.stars.lesson1.controller;


import com.example.stars.lesson1.domain.Shop;
import com.example.stars.lesson1.dto.request.PaginationRequestDTO;
import com.example.stars.lesson1.dto.request.ShopRequestDTO;
import com.example.stars.lesson1.dto.response.PageResponse;
import com.example.stars.lesson1.dto.response.ShopResponseDTO;
import com.example.stars.lesson1.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/shops")
public class ShopController {

    @Autowired
    private ShopService shopService;

    @PostMapping
    private ShopResponseDTO create(@RequestBody ShopRequestDTO shop) {
        return new ShopResponseDTO(shopService.save(shop));
    }

    @PutMapping("/{id}")
    private ShopResponseDTO update(@RequestBody ShopRequestDTO shop, @PathVariable("id") Long id) {
        return new ShopResponseDTO(shopService.update(shop, id));
    }

    @GetMapping("/{id}")
    private ShopResponseDTO get(@PathVariable("id") Long id) {
        return new ShopResponseDTO(shopService.getById(id));
    }

    @DeleteMapping("/{id}")
    private void delete(@PathVariable("id") Long id) {
        shopService.delete(id);
    }

    @GetMapping()
    private List<ShopResponseDTO> getAll() {
        return shopService.getAll()
                .stream()
                .map(ShopResponseDTO::new)
                .collect(Collectors.toList());
    }

    @GetMapping("/page")
    private PageResponse<ShopResponseDTO> getPage(PaginationRequestDTO paginationRequestDTO) {
        Page<Shop> page = shopService.getPage(paginationRequestDTO);
        return new PageResponse<>(
                page.get().map(ShopResponseDTO::new).collect(Collectors.toList()),
                page.getTotalElements(),
                page.getTotalPages()
        );
    }

    @GetMapping("/category/{categoryId}")
    private List<ShopResponseDTO> getAllByCategory(@PathVariable("categoryId") Long id) {
        return shopService.getAllByCategory(id)
                .stream()
                .map(ShopResponseDTO::new)
                .collect(Collectors.toList());
    }



}

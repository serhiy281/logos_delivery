package com.example.stars.lesson1.controller;

import com.example.stars.lesson1.domain.Item;
import com.example.stars.lesson1.dto.request.ItemRequestDTO;
import com.example.stars.lesson1.dto.request.ItemSearchRequestDTO;
import com.example.stars.lesson1.dto.request.PaginationRequestDTO;
import com.example.stars.lesson1.dto.response.ItemResponseDTO;
import com.example.stars.lesson1.dto.response.PageResponse;
import com.example.stars.lesson1.service.ItemService;
import com.example.stars.lesson1.tools.FileTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/items")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private FileTool fileTool;

//	@PostMapping("/image")
//	private String saveImage(@RequestBody String image) throws IOException {
//		return fileTool.saveFile(image);
//	}

    // CRUD - create read update delete
    @PostMapping
    private void createItem(@RequestBody ItemRequestDTO item) throws IOException {
        itemService.save(item);
    }

    @PutMapping("/{id}")
    private ItemResponseDTO updateItem(@RequestBody ItemRequestDTO item, @PathVariable("id") Long id) throws IOException {
        return new ItemResponseDTO(itemService.update(item, id));
    }

    @GetMapping("/{id}")
    private ItemResponseDTO getById(@PathVariable("id") Long id) {
        return new ItemResponseDTO(itemService.getById(id));
    }

    @GetMapping
    private PageResponse<ItemResponseDTO> getAll(@Valid ItemSearchRequestDTO searchRequest) {
        System.out.println(searchRequest.getPagination());
        Page<Item> page = itemService.getPageByShopId(searchRequest);
        return new PageResponse<>(
                page.get()
                        .map(ItemResponseDTO::new)
                        .collect(Collectors.toList()),
                page.getTotalElements(),
                page.getTotalPages()
        );
    }

    @DeleteMapping("/{id}")
    private void deleteItem(@PathVariable("id") Long id) {
        itemService.delete(id);
    }
}

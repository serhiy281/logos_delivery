package com.example.stars.lesson1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@CrossOrigin
public class HTMLPagesController {

    // localhost:8080/admin/shop
    @RequestMapping(value = "/admin/shop")
    private String getAdminShopPage() {
        return "/html/admin-shop.html";
    }

    @RequestMapping("/admin/shop-manage/{shopId}")
    private String getAdminShopManagePage() {
        return "/html/admin-shop-manage.html";
    }

    @RequestMapping("/registration")
    private String getUserRegistrationPage() {
        return "/html/user-registration.html";
    }

    @RequestMapping("/login")
    private String getLoginPage() {
        return "/html/login.html";
    }

    @RequestMapping
    private String getMainPage() {
        return "/html/index.html";
    }
}

package com.example.stars.lesson1.controller;

import com.example.stars.lesson1.dto.request.UserLoginRequestDTO;
import com.example.stars.lesson1.dto.request.UserRegistrationRequestDTO;
import com.example.stars.lesson1.dto.response.AuthenticationResponseDTO;
import com.example.stars.lesson1.dto.response.UserResponseDTO;
import com.example.stars.lesson1.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {

    private UserServiceImpl userServiceImpl;

    @Autowired
    public UserController(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @PostMapping("/register")
    public AuthenticationResponseDTO registerUser(@RequestBody UserRegistrationRequestDTO requestDTO) {
        return userServiceImpl.registerUser(requestDTO);
    }

    @PostMapping("/login")
    public AuthenticationResponseDTO login(@RequestBody UserLoginRequestDTO requestDTO) {
        return userServiceImpl.login(requestDTO);
    }
}

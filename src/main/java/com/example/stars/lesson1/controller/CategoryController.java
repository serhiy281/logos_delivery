package com.example.stars.lesson1.controller;

import com.example.stars.lesson1.dto.request.CategoryRequestDTO;
import com.example.stars.lesson1.dto.response.CategoryResponseDTO;
import com.example.stars.lesson1.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping
    private void createCategory(@RequestBody CategoryRequestDTO categoryRequestDTO) {
        categoryService.create(categoryRequestDTO);
    }

    @GetMapping
    private List<CategoryResponseDTO> getAll() {
        return categoryService.getAll().stream()
                .map(CategoryResponseDTO::new)
                .collect(Collectors.toList());
    }
}

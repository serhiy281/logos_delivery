package com.example.stars.lesson1.controller;

import com.example.stars.lesson1.dto.response.CartResponseDTO;
import com.example.stars.lesson1.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/carts")
public class CartController {

    private CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @PutMapping("/add-item")
    private void addItemToCart(Long id, Long itemId) {
        cartService.addItem(id, itemId);
    }

    @GetMapping
    private CartResponseDTO getCart() {
        return new CartResponseDTO(cartService.getByCurrentUser());
    }
}

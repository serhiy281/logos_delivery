package com.example.stars.lesson1.repository;

import com.example.stars.lesson1.domain.Cart;
import com.example.stars.lesson1.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

//    @Query(value = "select IF(count(*), true, false)\n" +
//            "from cart inner join item_count_cart_mapping on\n" +
//            "    cart.id = item_count_cart_mapping.cart_id where item_id = :itemId and cart_id = :cartId", nativeQuery = true)
//    boolean existsByItemId(@Param("cartId") Long cartId, @Param("itemId") Long itemId);

    @Query(
            "select case when count(KEY(items))> 0 then true else false end " +
                    "from Cart c join c.itemsCountMap items " +
                    "where c =:cart and KEY(items).id = :itemId"
    )
    boolean cartContainsItem(
            @Param("cart") Cart cart,
            @Param("itemId") Long item
    );

}

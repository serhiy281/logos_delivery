package com.example.stars.lesson1.repository;

import com.example.stars.lesson1.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


    boolean existsByLogin(String login);

    User findByLogin(String login);
}

package com.example.stars.lesson1.repository;

import com.example.stars.lesson1.domain.Category;
import com.example.stars.lesson1.domain.Shop;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {


}

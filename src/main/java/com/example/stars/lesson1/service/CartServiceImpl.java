package com.example.stars.lesson1.service;

import com.example.stars.lesson1.domain.Cart;
import com.example.stars.lesson1.domain.Item;
import com.example.stars.lesson1.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImpl implements CartService{

    private CartRepository cartRepository;

    private ItemService itemService;

    private UserService userService;

    @Autowired
    public CartServiceImpl(CartRepository cartRepository,
                           ItemService itemService,
                           UserService userService) {
        this.cartRepository = cartRepository;
        this.itemService = itemService;
        this.userService = userService;
    }

    @Override
    public void addItem(Long cartId, Long itemId) {
        Cart cart = getById(cartId);

        Item item = itemService.getById(itemId);
        cart.getItemsCountMap().putIfAbsent(item, 1);

        cartRepository.save(cart);
    }

    @Override
    public Cart getById(Long id) {
        return cartRepository.getById(id);
    }

    @Override
    public Cart getByCurrentUser() {
        return userService.getCurrentUser().getCart();
    }
}

package com.example.stars.lesson1.service;

import com.example.stars.lesson1.domain.Shop;
import com.example.stars.lesson1.dto.request.PaginationRequestDTO;
import com.example.stars.lesson1.dto.request.ShopRequestDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ShopService {


    Shop save(ShopRequestDTO shop);

    Shop update(ShopRequestDTO shop, Long id);

    Shop getById(Long id);

    List<Shop> getAll();

    List<Shop> getAllByCategory(Long categoryId);

    Page<Shop> getPage(PaginationRequestDTO paginationRequestDTO);

    void delete(Long id);
}

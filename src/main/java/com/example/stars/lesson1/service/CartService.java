package com.example.stars.lesson1.service;

import com.example.stars.lesson1.domain.Cart;

public interface CartService {

    void addItem(Long cartId, Long itemId);

    Cart getById(Long id);

    Cart getByCurrentUser();
}

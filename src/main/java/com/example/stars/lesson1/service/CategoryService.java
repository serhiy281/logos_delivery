package com.example.stars.lesson1.service;

import com.example.stars.lesson1.domain.Category;
import com.example.stars.lesson1.dto.request.CategoryRequestDTO;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CategoryService {


    void create(CategoryRequestDTO requestDTO);

    Category getById(Long id);

    List<Category> getAll();
}

package com.example.stars.lesson1.service;

import com.example.stars.lesson1.domain.User;
import com.example.stars.lesson1.dto.request.UserLoginRequestDTO;
import com.example.stars.lesson1.dto.request.UserRegistrationRequestDTO;
import com.example.stars.lesson1.dto.response.AuthenticationResponseDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {


  AuthenticationResponseDTO registerUser(UserRegistrationRequestDTO requestDTO);

  AuthenticationResponseDTO login(UserLoginRequestDTO requestDTO);

  User findByLogin(String username);

  User getCurrentUser();
}

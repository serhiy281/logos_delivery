package com.example.stars.lesson1.service;

import com.example.stars.lesson1.domain.Item;
import com.example.stars.lesson1.dto.request.ItemRequestDTO;
import com.example.stars.lesson1.dto.request.ItemSearchRequestDTO;
import com.example.stars.lesson1.repository.ItemRepository;
import com.example.stars.lesson1.specification.ItemSpecification;
import com.example.stars.lesson1.tools.FileTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


import java.io.IOException;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {
    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ShopService shopService;

    @Autowired
    private FileTool fileTool;

    @Override
    public void save(ItemRequestDTO itemRequestDTO) throws IOException {
        itemRepository.save(mapItemRequestToItem(itemRequestDTO, null));
    }

    @Override
    public Item getById(Long id) {
        return itemRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Item with id " + id + " doesn't exist"));
    }

    @Override
    public List<Item> getAll() {
        return itemRepository.findAll();
    }

    @Override
    public Page<Item> getPageByShopId(ItemSearchRequestDTO searchRequest) {
        return itemRepository.findAll(
                new ItemSpecification(searchRequest),
                searchRequest.getPagination().mapToPageable()
        );
    }

    @Override
    public Item update(ItemRequestDTO itemRequestDTO, Long id) throws IOException {
        return itemRepository.save(mapItemRequestToItem(itemRequestDTO, getById(id)));
    }

    @Override
    public void delete(Long id) {
        itemRepository.deleteById(id);
    }

    private Item mapItemRequestToItem(ItemRequestDTO itemRequestDTO, Item item) throws IOException {
        if (item == null) {
            item = new Item();
        }
        item.setName(itemRequestDTO.getName());
        item.setPrice(itemRequestDTO.getPrice());
        item.setCount(itemRequestDTO.getCount());
        item.setDescription(itemRequestDTO.getDescription());
        item.setShop(shopService.getById(itemRequestDTO.getShopId()));
        if (itemRequestDTO.getImage() != null) {
            item.setImage(fileTool.saveFile(itemRequestDTO.getImage()));
        }

        return item;
    }
}

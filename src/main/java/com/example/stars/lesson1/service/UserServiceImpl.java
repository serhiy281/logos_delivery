package com.example.stars.lesson1.service;

import com.example.stars.lesson1.domain.Cart;
import com.example.stars.lesson1.domain.User;
import com.example.stars.lesson1.domain.UserRole;
import com.example.stars.lesson1.dto.request.UserLoginRequestDTO;
import com.example.stars.lesson1.dto.request.UserRegistrationRequestDTO;
import com.example.stars.lesson1.dto.response.AuthenticationResponseDTO;
import com.example.stars.lesson1.repository.UserRepository;
import com.example.stars.lesson1.security.JwtTokenTool;
import com.example.stars.lesson1.security.JwtUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    //TODO 14
    private AuthenticationManager authenticationManager;

    private JwtTokenTool jwtTokenTool;

    private BCryptPasswordEncoder encoder;


    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           AuthenticationManager authenticationManager,
                           JwtTokenTool jwtTokenTool,
                           BCryptPasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtTokenTool = jwtTokenTool;
        this.encoder = encoder;
    }

    // TODO 15
    @Override
    public AuthenticationResponseDTO registerUser(UserRegistrationRequestDTO request) {
        if (userRepository.existsByLogin(request.getLogin())) {
            throw new BadCredentialsException(
                    "User with login " + request.getLogin() + " already exists");
        }
        User user = new User();
        user.setLogin(request.getLogin());
        user.setUserRole(UserRole.ROLE_USER);
        user.setPassword(encoder.encode(request.getPassword()));
        user.setUsername(request.getUsername());
        user.setCart(new Cart());

        userRepository.save(user);

        return login(mapRegistrationToLogin(request));
    }

    // TODO 16
    @Override
    public AuthenticationResponseDTO login(UserLoginRequestDTO request) {
        String login = request.getLogin();
        User user = findByLogin(login);

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, request.getPassword()));

        String token = jwtTokenTool.createToken(login, user.getUserRole());
        String name = user.getUsername();
        Long id = user.getId();

        return new AuthenticationResponseDTO(name, token, id);
    }

    private UserLoginRequestDTO mapRegistrationToLogin(UserRegistrationRequestDTO registration) {
        return new UserLoginRequestDTO(registration.getLogin(), registration.getPassword());
    }

    @Override
    public User findByLogin(String username) {
        return userRepository.findByLogin(username);
    }

    @Override
    public User getCurrentUser() {
        return findByLogin((String) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(login);
        return new JwtUser(user.getLogin(), user.getUserRole(), user.getPassword());
    }
}
// secret TODO 17 - make injection on fields

package com.example.stars.lesson1.domain;


public enum EntityStatus {
    ACTIVE, DELETED
}

package com.example.stars.lesson1.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer price;

    private Integer count;

    private String description;

    private LocalDateTime creationDate = LocalDateTime.now();

    private String image;

    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;


}


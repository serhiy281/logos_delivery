package com.example.stars.lesson1.dto.request;

import com.example.stars.lesson1.domain.Category;
import com.example.stars.lesson1.domain.Item;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Getter
public class ShopRequestDTO {

    private String name;

    private Set<Long> categoryIds = new HashSet<>();

}

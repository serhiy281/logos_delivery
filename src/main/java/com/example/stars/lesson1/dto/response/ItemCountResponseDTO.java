package com.example.stars.lesson1.dto.response;

import com.example.stars.lesson1.domain.Item;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemCountResponseDTO {
    private ItemResponseDTO item;

    private Integer count;

    public ItemCountResponseDTO(Item item, Integer count) {
        this.item = new ItemResponseDTO(item);
        this.count = count;
    }
}

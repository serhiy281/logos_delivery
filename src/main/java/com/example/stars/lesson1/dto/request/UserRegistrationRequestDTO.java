package com.example.stars.lesson1.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRegistrationRequestDTO {
    private String login;

    private String password;

    private String username;


}

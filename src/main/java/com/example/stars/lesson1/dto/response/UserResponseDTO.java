package com.example.stars.lesson1.dto.response;

import com.example.stars.lesson1.domain.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponseDTO {

    private String username;

    public UserResponseDTO(User user) {
        this.username = user.getUsername();
    }
}

package com.example.stars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LessonApplication {

    public static void main(String[] args) {
        SpringApplication.run(LessonApplication.class, args);
    }

    // http {"name":\n"text", "image" : "adfasdfsa" }
    // img -> Base64 -> backend -> decode -> save -> filename -> localhost:8080/.../filename
    //second commit
}
